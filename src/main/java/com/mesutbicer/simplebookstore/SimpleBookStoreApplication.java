package com.mesutbicer.simplebookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.mesutbicer.simplebookstore.repository")
@EntityScan("com.mesutbicer.simplebookstore.entity")
@SpringBootApplication
public class SimpleBookStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleBookStoreApplication.class, args);
    }

}
