package com.mesutbicer.simplebookstore.controller;

import com.mesutbicer.simplebookstore.entity.Book;
import com.mesutbicer.simplebookstore.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public Iterable<Book> findAll() {
        return bookService.tumKitapKayitlariniGetir();
    }

    @GetMapping("/title/{bookTitle}")
    public List<Book> findByTitle(@PathVariable String bookTitle) {
        return bookService.titleIBuOlanKitaplariGetir(bookTitle);
    }

    @GetMapping("/{id}")
    public Book findOne(@PathVariable Long id) {
        return bookService.IdSiBuOlanKitabiGetir(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Book create(@RequestBody Book book) {
        return bookService.kaydet(book);
    }

    @PutMapping("/{id}")
    public Book updateBook(@RequestBody Book updatedBook, @PathVariable Long id) {
        return bookService.IdSiBuOlanKitabiBuSekildeGuncelle(id,updatedBook);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        bookService.idSiBuOlanKitabiSil(id);
    }

}
