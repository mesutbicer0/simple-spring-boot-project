package com.mesutbicer.simplebookstore.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Difference between @Controller & @RestController
 * --------------------------------------------------
 *
 * #@Controller is used to mark classes as Spring MVC Controller.
 * #@RestController annotation is a special controller used in RESTful Web services,
 * and it's the combination of @Controller and @ResponseBody annotation.
 */

@Controller
public class SimpleController {
    @Value("${spring.application.name}")
    String appName;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        return "home";
    }
}
