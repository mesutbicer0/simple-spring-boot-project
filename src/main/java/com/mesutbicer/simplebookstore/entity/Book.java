package com.mesutbicer.simplebookstore.entity;

import jakarta.persistence.*;
import lombok.Data;

/**
 * Bazen entity paketi model diye de isimlendirilebilmektedir. !!!
 */

@Entity
@Data
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, unique = true)
    private String title;

    @Column(nullable = false)
    private String author;
}
