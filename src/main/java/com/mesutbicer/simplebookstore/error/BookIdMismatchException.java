package com.mesutbicer.simplebookstore.error;

public class BookIdMismatchException extends RuntimeException {
    public BookIdMismatchException() {
        super();
    }
}
