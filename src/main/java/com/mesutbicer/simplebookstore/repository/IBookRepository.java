package com.mesutbicer.simplebookstore.repository;

import com.mesutbicer.simplebookstore.entity.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IBookRepository extends CrudRepository<Book, Long> {
    List<Book> findByTitle(String title);
}
