package com.mesutbicer.simplebookstore.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest()
                .permitAll()
                .and()
                .csrf()
                .disable();

        //Source: https://stackoverflow.com/questions/47221582/h2-in-memory-database-console-not-opening
        // disable frame options (H2 Web Console baglanilamadi demesin diye)
        http.headers().frameOptions().disable();

        return http.build();
    }
}
