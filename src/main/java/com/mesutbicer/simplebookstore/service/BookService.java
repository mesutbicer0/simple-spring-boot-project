package com.mesutbicer.simplebookstore.service;

import com.mesutbicer.simplebookstore.entity.Book;
import com.mesutbicer.simplebookstore.error.BookIdMismatchException;
import com.mesutbicer.simplebookstore.error.BookNotFoundException;
import com.mesutbicer.simplebookstore.repository.IBookRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService implements IBookService{
    private final IBookRepository iBookRepository;

    public BookService(IBookRepository iBookRepository) {
        this.iBookRepository = iBookRepository;
    }

    @Override
    public Book kaydet(Book book) {
        return iBookRepository.save(book);
    }

    @Override
    //public List<Book> tumKitapKayitlariniGetir() {
    public Iterable<Book> tumKitapKayitlariniGetir() {
        return iBookRepository.findAll();
    }

    @Override
    public List<Book> titleIBuOlanKitaplariGetir(String bookTitle) {
        return iBookRepository.findByTitle(bookTitle);
    }

    @Override
    public Book IdSiBuOlanKitabiGetir(Long id) {
        //return iBookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        return iBookRepository.findById(id).orElseThrow(() ->
                new BookNotFoundException("error", new Throwable())
        );
    }

    @Override
    public Book IdSiBuOlanKitabiBuSekildeGuncelle(Long id, Book updatedBook) {
        if (updatedBook.getId() != id) {
            throw new BookIdMismatchException();
        }
        //iBookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        iBookRepository.findById(id).orElseThrow(() ->
                new BookNotFoundException("error", new Throwable())
        );
        return iBookRepository.save(updatedBook);
    }

    @Override
    public void idSiBuOlanKitabiSil(Long id) {
        //iBookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        iBookRepository.findById(id).orElseThrow(() ->
                new BookNotFoundException("error", new Throwable())
        );
        iBookRepository.deleteById(id);
    }
}
