package com.mesutbicer.simplebookstore.service;

import com.mesutbicer.simplebookstore.entity.Book;

import java.util.List;

public interface IBookService {
    //Create Operation
    Book kaydet(Book book);

    //Read Operations
    //List<Book> tumKitapKayitlariniGetir();
    Iterable<Book> tumKitapKayitlariniGetir();
    List<Book> titleIBuOlanKitaplariGetir(String title); // 1 den fazla olabileceginden
    Book IdSiBuOlanKitabiGetir(Long id);

    //Update Operation
    Book IdSiBuOlanKitabiBuSekildeGuncelle(Long id, Book updatedBook);

    //Delete Operation
    void idSiBuOlanKitabiSil(Long id);
}
